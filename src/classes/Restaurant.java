package Restaurant;

import java.util.Random;

import Enum.Experience;

public class Restaurant {
	private String name;

	private String address;

	private String city;

	private Player owner;

	private Integer budget;

	private Employee work;

	private Table generate;

	private Integer reputationPoints;

	private MenuItem finalize;

	private static Employee employees = new Employee();

	private MenuItem menuItems;

	private Order process;

	private Table tables;

	public void computeReputation(Experience barman, Experience chef,
			Experience waiter[])

	{
		if (barman == Experience.High) {
			Random random = new Random();
			int x = showRandomInteger(0, 100, random);
			if (x < 80) {
				reputationPoints++;
			} else {
				reputationPoints = reputationPoints - 1;
			}
		}

		if (barman == Experience.Medium) {
			Random random = new Random();
			int x = showRandomInteger(0, 100, random);
			if (x < 60) {
				reputationPoints++;
			} else {
				reputationPoints = reputationPoints - 1;
			}
		}
		if (barman == Experience.Low) {
			Random random = new Random();
			int x = showRandomInteger(0, 100, random);
			if (x < 40) {
				reputationPoints++;
			} else {
				reputationPoints = reputationPoints - 1;
			}
		}

		if (chef == Experience.High) {
			Random random = new Random();
			int x = showRandomInteger(0, 100, random);
			if (x < 80) {
				reputationPoints++;
			} else {
				reputationPoints = reputationPoints - 1;
			}
		}

		if (chef == Experience.Medium) {
			Random random = new Random();
			int x = showRandomInteger(0, 100, random);
			if (x < 60) {
				reputationPoints++;
			} else {
				reputationPoints = reputationPoints - 1;
			}
		}
		if (chef == Experience.Low) {
			Random random = new Random();
			int x = showRandomInteger(0, 100, random);
			if (x < 40) {
				reputationPoints++;
			} else {
				reputationPoints = reputationPoints - 1;
			}
		}

		for (i = 0; i < 3; i++) {

			if (waiter[i] == Experience.High) {
				Random random = new Random();
				int x = showRandomInteger(0, 100, random);
				if (x < 90) {
					reputationPoints++;
				} else {
					reputationPoints = reputationPoints - 1;
				}
			}
			if (waiter[i] == Experience.Medium) {
				Random random = new Random();
				int x = showRandomInteger(0, 100, random);
				if (x < 80) {
					reputationPoints++;
				} else {
					reputationPoints = reputationPoints - 1;
				}
			}

			if (waiter[i] == Experience.Low) {
				Random random = new Random();
				int x = showRandomInteger(0, 100, random);
				if (x < 40) {
					reputationPoints++;
				} else {
					reputationPoints = reputationPoints - 1;
				}
			}

		}

	}

	public boolean payTraining(Integer amount) {
		if (budget > amount)

		{
			budget = budget - amount;
			System.out.println("Player Trained");
			return true;

		} else {
			System.out.println("Not Enough Money");
		}
		return false;

	}

	MenuItem m;
	Order o[];
	int i;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getBudget() {
		return budget;
	}

	public void setBudget(Integer budget) {
		this.budget = budget;
	}

	public Integer getReputationPoints() {
		return reputationPoints;
	}

	public void setReputationPoints(Integer reputationPoints) {
		this.reputationPoints = reputationPoints;
	}

	private static int showRandomInteger(int aStart, int aEnd, Random aRandom) {
		if (aStart > aEnd) {
			throw new IllegalArgumentException("Start cannot exceed End.");
		}
		// get the range, casting to long to avoid overflow problems
		long range = (long) aEnd - (long) aStart + 1;
		// compute a fraction of the range, 0 <= frac < range
		long fraction = (long) (range * aRandom.nextDouble());
		int randomNumber = (int) (fraction + aStart);
		return randomNumber;

	}

}
