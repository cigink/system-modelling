package Restaurant;

import Enum.EmployeeType;
import Enum.Experience;

public class Employee {
	protected String name;

	protected String surname;

	protected Integer salary;

	protected Experience experience;

	protected EmployeeType type;

	public void computeSalary() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public void increaseExperience(String Employee) {
		this.experience = experience.High;
		System.out.println("Experience has been increased to HIGH");
	}

	public Experience getExperience() {
		return experience;
	}

	public void setExperience(Experience experience) {
		this.experience = experience;
	}

}
