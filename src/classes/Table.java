package Restaurant;

import Enum.Occupancy;

public class Table {
	private Integer number;
	private Occupancy occupancy;

	private Waiter assign;
	private Waiter waiter;

	private Client seating;

	private Restaurant process;

	public Table(int _number) {
		number = _number;
	}

	public static void assignToWaiter(Waiter waiter) {

	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Waiter getAssign() {
		return assign;
	}

	public void setAssign(Waiter assign) {
		this.assign = assign;
	}

	public Client getSeating() {
		return seating;
	}

	public void setSeating(Client seating) {
		this.seating = seating;
	}

	public Restaurant getProcess() {
		return process;
	}

	public void setProcess(Restaurant process) {
		this.process = process;
	}

	public Waiter getWaiter() {
		return waiter;
	}

	public void setWaiter(Waiter waiter) {
		this.waiter = waiter;
	}

	public Occupancy getOccupancy() {
		return occupancy;
	}

	public void setOccupancy(Occupancy occupancy) {
		this.occupancy = occupancy;
	}

}
