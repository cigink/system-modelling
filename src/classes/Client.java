package Restaurant;

public class Client {
	private Integer clientTable;

	private String name;

	private String surname;

	private Integer telephone;

	private Integer taxCode;

	private Statistics clientStatistics;

	private MenuItem clientMenuItem;

	public Client(String name) {
		this.name = name;
	}

	public void isSatisfied() {

	}

	public void computeStatistics() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getClientTable() {
		return clientTable;
	}

	public void setClientTable(Integer clientTable) {
		this.clientTable = clientTable;
	}

}
