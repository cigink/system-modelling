package Restaurant;

import java.util.Random;
import java.util.Scanner;

import Enum.Experience;
import Enum.Occupancy;

public class GameController

{
	private static Player player;
	private static Restaurant res = new Restaurant();
	private static MenuItem menu = new MenuItem();
	private static Waiter[] waiter = new Waiter[3];
	private static Barman barman = new Barman();
	private static Chef chef = new Chef();
	private static MainDish[] dish = new MainDish[5];

	private static Beverage[] bev = new Beverage[5];

	private static Client client[] = new Client[18];
	// private Restaurant restaurant = new Restaurant
	// (tables,waiter,chef,barmen)
	private RankingList playerRankingList = new RankingList();

	public static void main(String[] args)

	{
		chooseName(); // Set restaurant name, address and other details

		startGame(); // Start the game

	}

	public static void chooseName() {

		Scanner user_input = new Scanner(System.in);

		System.out.println("Enter name of the restaurant ");
		res.setName(user_input.nextLine());
		System.out.println(res.getName() + " was chosen");

		System.out.println("Enter name of the city ");
		res.setCity(user_input.nextLine());
		System.out.println(res.getCity() + " was chosen");

		System.out.println("Enter address of the restaurant ");
		res.setAddress(user_input.nextLine());
		System.out.println(res.getAddress() + " was chosen");

		// user_input.close();// close the scanner
	}

	private static void ingrdntcost(int a, int b, int b1, int b2) {
		int hqdish = 10 * a;
		int lqdish = 10 * b;
		int hqbev = 3 * b1;
		int lqbev = 1 * b2;
		int total = hqdish + lqdish + hqbev + lqbev;

		int z = res.getBudget() - total;
		res.setBudget(z);

	}

	public static void startGame() {
		int currentDay = 1;
		Restaurant res = new Restaurant();
		res.setBudget(10000);
		res.setReputationPoints(15); // Default values
		Barman b = new Barman();
		Waiter w[] = new Waiter[3];
		Chef c = new Chef();

		float ingridient = 0;
		while (res.getBudget() != 0 && currentDay <= 30) {

			Scanner user_input = new Scanner(System.in);
			// Weekly Task
			if (currentDay % 7 == 0) {
				System.out.println("Pay salary of Barman Chef and Waiter");
				int barmanSal = res.getBudget() - b.salary;
				res.setBudget(barmanSal);

				int chefsal = res.getBudget() - c.salary;
				res.setBudget(chefsal);

				for (int i = 1; i <= 3; i++) {
					int wait = res.getBudget() - w[i].salary;
					res.setBudget(wait);
				}
			}

			// System.out.println("Pay ingridient cost");

			// Monthly Task

			if (currentDay % 30 == 0) {
				int maintainance = res.getBudget() - 4000;
				res.setBudget(maintainance);
			}

			System.out.println("Type YES to enter the game else type NO");
			String x = user_input.nextLine();
			if (x.equals("YES")) {

				// res.setBudget(10000); // Budget amount is set

				System.out
						.println("Enter your choice: 1. Assign Table \n 2. Define Menu \n 3. Train Employee ");
				int choice = new Integer(user_input.nextLine());
				switch (choice) {
				case 1:

					System.out.println("Choice 1: Assign Table");
					System.out.println("Enter waiter name");
					waiter[1].setName(user_input.nextLine());

					waiter[2].setName(user_input.nextLine());

					waiter[3].setName(user_input.nextLine());

					Table tablesAssigned[] = new Table[3];
					tablesAssigned = new Table[] { new Table(1), new Table(2),
							new Table(3) };
					waiter[1].setAssign(tablesAssigned);
					tablesAssigned = new Table[] { new Table(4), new Table(5),
							new Table(6) };
					waiter[2].setAssign(tablesAssigned);
					tablesAssigned = new Table[] { new Table(7), new Table(8),
							new Table(9) };
					waiter[3].setAssign(tablesAssigned); // no error
															// handling
															// implemented
					break;
				case 2:
					// setting the menu
					System.out.println("Choice 2: Defining menu");
					System.out.println("Enter number of High quality dishes ");
					int a1,
					b1,
					a2,
					b2;
					a1 = new Integer(user_input.nextLine());
					System.out.println("Enter number of Low quality dishes ");
					b1 = new Integer(user_input.nextLine());
					setDishesQuality(a1, b1);
					System.out
							.println("Enter number of High quality beverages ");
					a2 = new Integer(user_input.nextLine());
					System.out
							.println("Enter number of Low quality beverages ");
					b2 = new Integer(user_input.nextLine());
					setBeveragesQuality(a2, b2);
					ingrdntcost(a1, a2, b1, b2);

					break;
				case 3:
					System.out.println("Choice 3 : Employee Training");

					System.out
							.println("Enter name for training (1 Waiter1/ 2 Waiter2/ 3 Waiter3/ 4 barman/ 5 chef)");
					int e = new Integer(user_input.nextLine());
					switch (e) {
					case 1:
						switch (waiter[1].getExperience()) {
						case Low:
							if (res.payTraining(800)) {
								waiter[1].setExperience(Experience.Medium);
								waiter[1].setSalary(300);
							}
							break;
						case Medium:
							if (res.payTraining(800)) {
								waiter[1].setExperience(Experience.High);
								waiter[1].setSalary(400);
							}
							break;
						case High:
							// do nothing
							break;
						}
						break;
					case 2:
						switch (waiter[2].getExperience()) {
						case Low:
							if (res.payTraining(800)) {
								waiter[2].setExperience(Experience.Medium);
								waiter[2].setSalary(300);

							}
							break;
						case Medium:
							if (res.payTraining(800)) {
								waiter[2].setExperience(Experience.High);
								waiter[2].setSalary(400);
							}
							break;
						case High:
							// do nothing
							break;
						}

						break;
					case 3:
						switch (waiter[3].getExperience()) {
						case Low:
							if (res.payTraining(800)) {
								waiter[3].setExperience(Experience.Medium);
								waiter[3].setSalary(300);
							}
							break;

						case Medium:
							if (res.payTraining(800)) {
								waiter[3].setExperience(Experience.High);
								waiter[3].setSalary(400);
							}
							break;
						case High:
							// do nothing
							break;
						}
						break;
					case 4:
						switch (chef.getExperience()) {
						case Low:
							if (res.payTraining(1200)) {
								chef.setExperience(Experience.Medium);
								chef.setSalary(400);
							}
							break;

						case Medium:
							if (res.payTraining(1200)) {
								chef.setExperience(Experience.High);
								chef.setSalary(500);
							}
							break;
						case High:
							// do nothing
							break;
						}
						break;
					case 5:
						switch (barman.getExperience()) {
						case Low:
							if (res.payTraining(1200)) {
								barman.setExperience(Experience.Medium);
								barman.setSalary(400);
							}
							break;
						case Medium:
							if (res.payTraining(1200)) {
								barman.setExperience(Experience.High);
								barman.setSalary(500);
							}
							break;
						case High:
							// do nothing
							break;
						}
						break;
					}

					break;
				}

			}
			user_input.close();// close the scanner
			currentDay += 1;
		}
	}

	public static void makeSelection(Waiter waiter[], Table table) {
		// Assign the waiter to the table
		table.setWaiter(waiter[1]);
		table.setWaiter(waiter[2]);
		table.setWaiter(waiter[3]);

	}

	public static void setDishesQuality(Integer highNo, Integer lowNo) {
		Scanner user_input = new Scanner(System.in);

		for (int i = 1; i <= highNo; i++) {
			System.out.print("Enter Name of High Quality Dish");
			dish[i].setName(user_input.nextLine());
			System.out.print("Enter price of High Quality Dish of Dish");
			dish[i].setPrice(new Integer(user_input.nextLine()));
			dish[i].setIngridientCost(10);
			System.out.print("Enter calorie count");
			dish[i].setCalorieCount((new Integer(user_input.nextLine())));
		}

		for (int i = 5 - highNo; i <= 5; i++) // To be reviewed
		{
			System.out.print("Enter Name of Low Quality Dish");
			dish[i].setName(user_input.nextLine());
			System.out.print("Enter price of Low Quality Dish of Dish");
			dish[i].setPrice(new Integer(user_input.nextLine()));
			dish[i].setIngridientCost(10);
			System.out.print("Enter calorie count");
			dish[i].setCalorieCount((new Integer(user_input.nextLine())));
		}
	}

	public static void setBeveragesQuality(Integer highNo, Integer lowNo) {
		Scanner user_input = new Scanner(System.in);

		for (int i = 0; i < highNo; i++) {
			System.out.print("Enter Name of High Quality Beverage");
			bev[i].setName(user_input.nextLine());
			System.out.print("Enter price of High Quality Beverage of Dish");
			bev[i].setPrice(new Integer(user_input.nextLine()));
			bev[i].setIngridientCost(3);
			System.out.print("Enter Volume of Beverage");
			bev[i].setVolume(new Integer(user_input.nextLine()));

		}

		for (int i = 5 - highNo; i < 5; i++) // Review For Loop
		{
			System.out.print("Enter Name of Low Quality Beverage");
			bev[i].setName(user_input.nextLine());
			System.out.print("Enter price of Low Quality Beverage");
			bev[i].setPrice(new Integer(user_input.nextLine()));
			bev[i].setIngridientCost(3);
			System.out.print("Enter Volume of Beverage");
			bev[i].setVolume(new Integer(user_input.nextLine()));

		}
	}

	public static Client[] clientpopulate(Client[] client) {
		for (int i = 0; i < 18; i++) {
			client[i] = new Client("Client " + i);
		}
		return client;
	}

	public void populateTables() {

		Table t[] = new Table[9];
		Client c[] = new Client[2];
		MainDish[] m = new MainDish[2];
		Beverage[] b = new Beverage[2];

		if (res.getReputationPoints() < 15) {

			Order[] order = new Order[2];
			for (int i = 1; i <= 2; i++)

			{
				t[i] = t[new Random().nextInt(t.length)];
				t[i].setOccupancy(Occupancy.Occupied);
				order[i].setTableno(t[i]);
				for (int j = 1; j <= 2; j++) {
					c[j] = c[new Random().nextInt(c.length)];
					c[j].setClientTable(t[j].getNumber());
					m[j] = m[new Random().nextInt(m.length)];
					b[j] = b[new Random().nextInt(b.length)];
					order[j].setBeverage(b[j]);
					order[j].setDish(m[j]);

				}

			}
		}

		else if (res.getReputationPoints() > 15
				&& res.getReputationPoints() < 30) {

			Order[] order = new Order[5];
			for (int i = 1; i <= 5; i++)

			{
				t[i] = t[new Random().nextInt(t.length)];
				t[i].setOccupancy(Occupancy.Occupied);
				order[i].setTableno(t[i]);
				for (int j = 1; j <= 2; j++) {
					c[j] = c[new Random().nextInt(c.length)];
					c[j].setClientTable(t[j].getNumber());
					m[j] = m[new Random().nextInt(m.length)];
					b[j] = b[new Random().nextInt(b.length)];
					order[j].setBeverage(b[j]);
					order[j].setDish(m[j]);

				}

			}
		}

		else if (res.getReputationPoints() > 30) {

			Order[] order = new Order[5];
			for (int i = 1; i <= 9; i++)

			{
				t[i] = t[new Random().nextInt(t.length)];
				t[i].setOccupancy(Occupancy.Occupied);
				order[i].setTableno(t[i]);
				for (int j = 1; j <= 2; j++) {
					c[j] = c[new Random().nextInt(c.length)];
					c[j].setClientTable(t[j].getNumber());
					m[j] = m[new Random().nextInt(m.length)];
					b[j] = b[new Random().nextInt(b.length)];
					order[j].setBeverage(b[j]);
					order[j].setDish(m[j]);

				}

			}
		}

	}

}
